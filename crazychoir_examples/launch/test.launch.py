from launch import LaunchDescription           # launch文件的描述类
from launch_ros.actions import Node            # 节点启动的描述类
import sys
sys.path.append('/home/issl/ws_crazy/src/crazychoir_examples/launch')

import get_n2
import Lib_Init
import Lib_Shape
Run_time = 24
Swarm_size = 10
Sim_range = [-2.5, 2.5, -2, 2]
Sim_param = Lib_Init.InitSimuParam(Run_time, Swarm_size)
size = Sim_param['swarm_size']

def generate_launch_description():             # 自动生成launch文件的函数
    Swarm_size = 10
    return LaunchDescription([                 # 返回launch文件的描述信息
        Node(                                  # 配置一个节点的启动
            package='crazychoir_examples',          # 节点所在的功能包
            executable='crazychoir_formation_webots_helloword', # 节点的可执行文件
            namespace='xiao',
            output='screen',
        ),
        # Node(
        #         package='crazychoir_examples', 
        #         executable='crazychoir_formation_webots_Fcn_SendNeighborSet',
        #         output='screen',
        #         namespace='xiao',
        #         parameters=[{
        #             'Sim_param': size,
        #             'Swarm_size': Swarm_size,
        #             }]
        #             ),
        Node(
                package='crazychoir_examples', 
                executable='crazychoir_formation_webots_Fcn_GetMsg',
                output='screen',
                namespace='xiao',
                    ),


    ])
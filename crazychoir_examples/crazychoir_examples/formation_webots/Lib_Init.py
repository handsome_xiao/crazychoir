import numpy as np

def InitSimuParam(run_time, swarm_size):
    param = {}
    param['time'] = run_time
    param['t'] = 0.01
    param['max_step'] = int(np.ceil(param['time'] / param['t']))
    param['r_body'] = 0.08
    param['r_safe'] = 0.10
    param['r_avoid'] = 0.6
    param['r_stab'] = 0.3
    param['r_sense'] = 2.5
    param['vel_max'] = 0.5
    param['swarm_size'] = swarm_size
    return param

def InitRecordState(sim_param, robot_state, shape_state, rcent_state, cmd_set):
    swarm_size = sim_param['swarm_size']
    max_step = sim_param['max_step']
    state = {}
    state['pset_rec'] = np.zeros((2, swarm_size, max_step+1))
    state['vset_rec'] = np.zeros((2, swarm_size, max_step+1))
    state['ptra_rec'] = np.zeros((2, max_step+1, swarm_size))
    state['vtra_rec'] = np.zeros((2, max_step+1, swarm_size))
    state['cmd_rec'] = np.zeros((2, max_step+1, swarm_size))
    state['pcen_rec'] = np.zeros((2, max_step+1))
    state['vcen_rec'] = np.zeros((2, max_step+1))
    state['hecen_rec'] = np.zeros((1, max_step+1))
    state['hvcen_rec'] = np.zeros((1, max_step+1))
    state['pest_rec'] = np.zeros((2, max_step+1, swarm_size))
    state['vest_rec'] = np.zeros((2, max_step+1, swarm_size))
    state['aest_rec'] = np.zeros((2, max_step+1, swarm_size))
    state['heest_rec'] = np.zeros((max_step+1, swarm_size))
    state['hvest_rec'] = np.zeros((max_step+1, swarm_size))
    state['haest_rec'] = np.zeros((max_step+1, swarm_size))
    state['ent_rate'] = np.zeros(max_step+1)
    state['dist_var'] = np.zeros(max_step+1)

    state['pset_rec'][:,:,0] = robot_state['pos_set']
    state['vset_rec'][:,:,0] = robot_state['vel_set']
    state['ptra_rec'][:,0,:] = robot_state['pos_set']
    state['vtra_rec'][:,0,:] = robot_state['vel_set']
    state['cmd_rec'][:,0,:] = cmd_set

    state['pcen_rec'][:,0] = rcent_state['pos_set']
    state['vcen_rec'][:,0] = rcent_state['vel_set']
    state['hecen_rec'][:,0] = rcent_state['head_set']
    state['hvcen_rec'][:,0] = rcent_state['hvel_set']

    state['pest_rec'][:,0,:] = shape_state['pos_set']
    state['vest_rec'][:,0,:] = shape_state['vel_set']
    state['aest_rec'][:,0,:] = shape_state['acc_set']
    state['heest_rec'][0,:] = shape_state['head_set']
    state['hvest_rec'][0,:] = shape_state['hvel_set']
    state['haest_rec'][0,:] = shape_state['hacc_set']

    return state

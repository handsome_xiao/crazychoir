#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
@作者: 古月居(www.guyuehome.com)
@说明: ROS2话题示例-发布“Hello World”话题
"""

import rclpy                                     # ROS2 Python接口库
from rclpy.node import Node                      # ROS2 节点类
from std_msgs.msg import String ,Float32MultiArray,Int32                 # 字符串消息类型
from cppmsg.msg import Matrix,AllRobotState
from geometry_msgs.msg import Pose
import numpy as np
import sys
import matplotlib.pyplot as plt
from geometry_msgs.msg import Vector3
# 添加其他路径到系统路径
# sys.path.append('/home/issl/ws_crazy/build/crazychoir/crazychoir/guidance')
from .xiao_4_force import Cal_4_force
from . import Lib_Init ,Lib_Update ,Lib_Shape
from . import Fcn_EstimShapeOrien,Fcn_EstimShapePosti,Fcn_GetNeighborSet,Lib_Curves
"""
创建一个发布者节点
"""
class PublisherNode(Node):
    
    def __init__(self, name):
        # xiaojvlong init
        super().__init__(name)
        self.Run_time = 10
        self.Swarm_size = 10
        self.Sim_range = [-2.5, 2.5, -2, 2]
        self.Sim_param = Lib_Init.InitSimuParam(self.Run_time, self.Swarm_size)
        self.scale =2
        # initialize robots' states (including position and velocity)
        self.Robot_state = {
            'pos_set': np.array([[-0.6, 0.5], [0, 0.6], [0.5, 0.5], [-0.7, -0.1], [0.2, -0.1],
                                [0.6, 0.1], [-0.6, -0.6], [0, -0.5], [0.6, -0.6], [-0.2, 0.1]]).T,
            'vel_set': np.zeros((2, self.Swarm_size)),
            'acc_set': np.zeros((2, self.Swarm_size)),
            'head_set': np.zeros(self.Swarm_size),
            'hvel_set': np.zeros(self.Swarm_size)
        }
        # type: 1-T, 2-I, 3-E
        self.type = 1
        self.step = 0
        # load shape image
        image = Lib_Shape.LoadShapeImage(self.type)
        self.Gray_image = image['gray_mtr']
        
        # initialize formation shape
        self.Gray_mtr, self.Gray_info = Lib_Shape.InitFormation(self.Sim_param, self.Gray_image)
        # initialize shape states
        self.Shape_state = Lib_Shape.InitShapeState(self.Sim_param, self.Robot_state)
        self.Rcent_state = Lib_Shape.GetShapeCenter(self.Swarm_size, self.Shape_state)



        self.pub = self.create_publisher(Vector3, "cmd_set", 10)   # 创建发布者对象（消息类型、话题名、队列长度）
        self.timer = self.create_timer(0.5, self.timer_callback)  # 创建一个定时器（单位为秒的周期，定时执行的回调函数）
        
    def timer_callback(self):                                     # 创建定时器周期执行的回调函数
        temp = self.evaluate_input_copy()
        temp = temp[:,5]
        # self.get_logger().info('temp: "%s"' % str(temp))
        # temp = [1.0, 1.75, 2.0, 3.0,4.1 ,5.2,6.3,9.6,8.5,7.2]
        msg = Vector3()                                # 填充消息对象中的消息数据
        # 分别对应i px py vx vy ax ay h w ha
        # msg.data = [1.0, 1.75, 2.0, 3.0,4.1 ,5.2,6.3,9.6,8.5,7.2]
        msg.x = temp[0]
        msg.y = temp[1]
        msg.z = 5.0
        # msg.vy = temp
        # msg.ax = temp
        # msg.ay = temp
        # msg.head = temp
        # msg.hv = temp
        # msg.ha = temp
        # msg.data = 32
        
        self.pub.publish(msg)                                     # 发布话题消息
        # self.get_logger().info('cmd_set: "%s"' % str(msg))     # 输出日志信息，提示已经完成话题发布



    def evaluate_input_copy(self):
        deform_step1 = 1000
        deform_step2 = 1500
        self.step += 1
        result = np.zeros((2, 1)) 
        for step in range(1):
            
            # if step == deform_step1:
            #     # Lib_Curves.DrawFinalSwarm(step , Sim_param, Record_state)
            #     Gray_image = Lib_Shape.LoadShapeImage(2)
            #     Gray_image = Gray_image['gray_mtr']
            #     Gray_mtr, Gray_info = Lib_Shape.InitFormation(self.Sim_param, Gray_image)
            # if step == deform_step2:
            #     # Lib_Curves.DrawFinalSwarm(step , Sim_param, Record_state)
            #     Gray_image = Lib_Shape.LoadShapeImage(3)
            #     Gray_image = Gray_image['gray_mtr']
            #     Gray_mtr, Gray_info = Lib_Shape.InitFormation(self.Sim_param, Gray_image)

            # get neighboring set
            Neigh_mtr = Fcn_GetNeighborSet.Fcn_GetNeighborSet(self.Sim_param, self.Robot_state)
            # estimate shape position
            state = Fcn_EstimShapePosti.Fcn_EstimShapePosti(self.Sim_param, Neigh_mtr, self.Shape_state)
            self.Shape_state['pos_set'] = state['pos_set']
            self.Shape_state['vel_set'] = state['vel_set']
            self.Shape_state['acc_set'] = state['acc_set']

            # estimate shape orientation
            state = Fcn_EstimShapeOrien.Fcn_EstimShapeOrien(self.Sim_param, Neigh_mtr, self.Shape_state)
            self.Shape_state['head_set'] = state['head_set']
            self.Shape_state['hvel_set'] = state['hvel_set']
            self.Shape_state['hacc_set'] = state['hacc_set']



            # get dynamic formation
            Rcent_state = Lib_Shape.GetShapeCenter(self.Swarm_size, self.Shape_state)
            Form_dyn = Lib_Shape.GetDynFormation(self.Swarm_size, self.Gray_mtr, self.Gray_info, self.Shape_state)
            # Lib_Curves.ShowMatrix(Form_dyn['shape_value'])

            
            # self.get_logger().info("第%s个智能体,第%s次循环" % (str(self.agent_id),str(self.step)))
            robot_state, vel_cmd, cmd_set = Cal_4_force(self.Sim_param, self.Robot_state, self.Shape_state, Form_dyn, self.Gray_info,Neigh_mtr,self.Swarm_size) 
            # # update robots' motion
            # robot_state['pos_set'] = Lib_Update.UpdRobotMotion(self.Sim_param, robot_state, vel_cmd)


            # self.Robot_state = robot_state
            # # 动态显示
            # if step%50 == 0 & self.agent_id == 2:
            # # if self.step%50 == 0:
            #     self.get_logger().info("显示当前飞机队形")
            #     # Lib_Curves.DrawScatter(range(10) , Shape_state['head_set'])
            #     if self.agent_id == 10:
            #         count_id = 9
            #     else:
            #         count_id = self.agent_id
            #     result = cmd_set[:,count_id]
                
            #     # self.uu = np.concatenate((u_temp, b))
                
            #     Lib_Curves.DrawScatter(self.Robot_state['pos_set'][0, :] , self.Robot_state['pos_set'][1, :])
            # self.get_logger().info("My control u is:\n %s" % str(cmd_set))

        
        # if self.agent_id == 2:
        # Lib_Curves.DrawRobotState(self.Robot_state)
        return cmd_set
        
def main(args=None):                                 # ROS2节点主入口main函数
    rclpy.init(args=args)                            # ROS2 Python接口初始化
    node = PublisherNode("topic_helloworld_pub")     # 创建ROS2节点对象并进行初始化
    rclpy.spin(node)                                 # 循环等待ROS2退出
    # node.destroy_node()                              # 销毁节点对象
    rclpy.shutdown()                                 # 关闭ROS2 Python接口
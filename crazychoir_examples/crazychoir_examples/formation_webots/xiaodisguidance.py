import rclpy
from rclpy.node import Node
from crazychoir.guidance import XiaoDisBearingFormation
import time
import threading
from geometry_msgs.msg import Vector3



        


def main():
    rclpy.init()
  
    frequency = 100

    guidance = XiaoDisBearingFormation(update_frequency=frequency, pose_handler='pubsub', pose_topic='odom')
    # litsten_thread = threading.Thread(target=guidance.litsten_thread)
    # litsten_thread.start()
    # guidance.get_logger().info('Go!')
    rclpy.spin(guidance)
    rclpy.shutdown()

import rclpy
# from rclpy.parameter import Parameter
import numpy as np


from rclpy.node import Node
from crazychoir.guidance import BearingFormation
import time
import numpy as np
from geometry_msgs.msg import Vector3
from sensor_msgs.msg import Image


import sys
# sys.path.insert(0, '/home/issl/ws_crazy/install/crazychoir_examples/lib/python3.8/site-packages/crazychoir_examples/msg')
# sys.path.append('/home/issl/ws_crazy/install/crazychoir_examples/lib/python3.8/site-packages')
from cppmsg.msg import Matrix
from std_msgs.msg import Float32MultiArray


class Fcn_GetNeighborSet(Node):
    def __init__(self, name):
        super().__init__(name, allow_undeclared_parameters=True,
            automatically_declare_parameters_from_overrides=True)
        # self.Sim_param = self.get_parameter('Sim_param').value
        self.Swarm_size = self.get_parameter('Swarm_size').value
        self.Sim_param = self.get_parameter('Sim_param').value
        self.size = self.Sim_param
        self.publishers_traj_params = self.create_publisher(Float32MultiArray, 'xiao_a_matrix', 1)
        self.timer = self.create_timer(1, self.timer_callback)  # 创建一个定时器（单位为秒的周期，定时执行的回调函数）
    def timer_callback(self):                                     # 创建定时器周期执行的回调函数
        # msg = Vector3()
        # msg.x       = self.size*0.2
        # msg.y       = 2.5
        # msg.z       = 3.5                                # 填充消息对象中的消息数据
        dd = np.array([[0.        , 0.59939567, 1.08343203, 0.59951256, 0.98969701,
                1.2622565 , 1.08702449, 1.15540777, 1.60687326, 0.55130387],
            [0.59939567, 0.        , 0.49950559, 0.97633271, 0.72036157,
                0.78733659, 1.32521739, 1.09333648, 1.32527709, 0.53107284],
            [1.08343203, 0.49950559, 0.        , 1.32845891, 0.67244945,
                0.43447057, 1.54451417, 1.1230327 , 1.10460184, 0.80492877],
            [0.59951256, 0.97633271, 1.32845891, 0.        , 0.89202471,
                1.30769121, 0.50564795, 0.797228  , 1.37563296, 0.52661428],
            [0.98969701, 0.72036157, 0.67244945, 0.89202471, 0.        ,
                0.44361952, 0.93427397, 0.4506565 , 0.63000112, 0.45182206],
            [1.2622565 , 0.78733659, 0.43447057, 1.30769121, 0.44361952,
                0.        , 1.37559454, 0.84508108, 0.68024769, 0.80730356],
            [1.08702449, 1.32521739, 1.54451417, 0.50564795, 0.93427397,
                1.37559454, 0.        , 0.59576256, 1.18419245, 0.79630269],
            [1.15540777, 1.09333648, 1.1230327 , 0.797228  , 0.4506565 ,
                0.84508108, 0.59576256, 0.        , 0.60272996, 0.63512914],
            [1.60687326, 1.32527709, 1.10460184, 1.37563296, 0.63000112,
                0.68024769, 1.18419245, 0.60272996, 0.        , 1.05652857],
            [0.55130387, 0.53107284, 0.80492877, 0.52661428, 0.45182206,
                0.80730356, 0.79630269, 0.63512914, 1.05652857, 0.        ]])
        a_temp = dd <= (np.ones((10, 10)) * 2.5)
        matrix = a_temp - np.eye(10)
        # matrix = np.ones((3, 3), dtype=int)
        # matrix[0,0]=5
        # matrix = np.array([[1, 2, 3],
        #            [4, 5, 6],
        #            [7, 8, 9]])
        array = matrix.flatten()
        array = array.astype(int)

        my_list = array.tolist()
        msg2 = Matrix()
        msg2.cols = len(my_list)
        msg2.rows = 4
        # msg2.data = [1,2,3,4,5,6,7,8,9]
        msg2.data = my_list
        msg3 = Float32MultiArray()
        # 分别对应i px py vx vy ax ay h w ha
        msg3.data = [1.0, 1.75, 2.0, 3.0,4.1 ,5.2,6.3,9.6,8.5,7.2]
        self.publishers_traj_params.publish(msg3)
        print("nihaonihao"*10)
        # self.get_logger().info("Published log mag2")
        # self.get_logger().info('My 邻居矩阵 is: %s,矩阵长度为%d' % (str(msg2.data),msg2.cols))
        self.get_logger().info('My Published is: %s' % str(msg3.data))
        # self.get_logger().info('My Published is: %d and %d' % (msg2.cols, msg2.rows))
        # log_msg = "Vector3: (%f, %f, %f)" % (msg.x, msg.y, msg.z)
        # self.get_logger().info(log_msg)
        # print("Vector3: ({}, {}, {})".format(msg.x, msg.y, msg.z))





def main(args=None):                                 # ROS2节点主入口main函数
    rclpy.init(args=args)                            # ROS2 Python接口初始化
    GetNeigh = Fcn_GetNeighborSet("topic_swarm_size")
    rclpy.spin(GetNeigh)
    rclpy.shutdown()








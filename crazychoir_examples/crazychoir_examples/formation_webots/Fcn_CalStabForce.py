import numpy as np

def Fcn_CalStabForce(sim_param, range_val, inside_set, robot_state, shape_mtr):
    # set proportional gain
    kappa_1 = 40.0
    kappa_2 = 40.0
    
    # variable substitution
    swarm_size = sim_param['swarm_size']
    pos_set = robot_state['pos_set']
    shape_x = shape_mtr['shape_x']
    shape_y = shape_mtr['shape_y']
    shape_value = shape_mtr['shape_value']
    
    # calculate stabilizing force
    force = np.zeros((2, swarm_size))
    
    for index in range(swarm_size):
        # get necessary matrices and sets
        px_set = pos_set[0,index]
        py_set = pos_set[1,index]
        fx_mtr = shape_x[:,:,index]
        fy_mtr = shape_y[:,:,index]
        gx_set = fx_mtr[shape_value == 0]
        gy_set = fy_mtr[shape_value == 0]
        
        # get relative distance between robot and black cells
        px_mtr = np.kron(px_set.reshape((-1,1)), np.ones((1, len(gx_set))))
        py_mtr = np.kron(py_set.reshape((-1,1)), np.ones((1, len(gy_set))))
        px_set = np.array([px_set])
        py_set = np.array([py_set])
        # print(len(px_set))
        gx_mtr = np.kron(np.ones((len(px_set), 1)), gx_set.reshape((1,-1)))
        gy_mtr = np.kron(np.ones((len(py_set), 1)), gy_set.reshape((1,-1)))
        delta_x = gx_mtr - px_mtr
        delta_y = gy_mtr - py_mtr
        dist_mtr = np.sqrt(delta_x**2 + delta_y**2)
        a_mtr = dist_mtr <= np.ones(dist_mtr.shape) * range_val
        a_mtr = a_mtr.astype(int)
        num_set = np.sum(a_mtr, axis=1)
        
        if num_set == 0:
            continue
        
        if inside_set[index] == 1:
            kappa = kappa_1
        else:
            kappa = kappa_2
            
        # calculate force
        glx_mtr = gx_mtr * a_mtr
        gly_mtr = gy_mtr * a_mtr
        avx_set = np.sum(glx_mtr, axis=1) / num_set
        avy_set = np.sum(gly_mtr, axis=1) / num_set
        force[0,index] = kappa * (avx_set - px_set)
        force[1,index] = kappa * (avy_set - py_set)
    
    return force

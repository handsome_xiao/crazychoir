import rclpy
# from rclpy.parameter import Parameter
from rclpy.node import Node
from crazychoir.guidance import BearingFormation
import time
import numpy as np
from geometry_msgs.msg import Vector3
from sensor_msgs.msg import Image

from cppmsg.msg import Matrix,AllRobotState


class Fcn_Getmsg(Node):
    def __init__(self, name):
        super().__init__(name, allow_undeclared_parameters=True,
            automatically_declare_parameters_from_overrides=True)  
        self.cmd_set = Vector3()     
        self.litsten = self.create_subscription(AllRobotState, '/xiao/single_robot_state', self.timer_callback ,2)
        self.litsten
        self.litsten2 = self.create_subscription(Vector3, "cmd_set", self.litsten_callback ,10)
        # self.litsten2
        
        # self.timer = self.create_timer(0.5, self.timer_callback)  # 创建一个定时器（单位为秒的周期，定时执行的回调函数）

    def litsten_callback(self,msg2):                                  

        # self.get_logger().info("recieved log mag2")
        # self.get_logger().info('My litiener is: %d and %d' % (msg2.cols, msg2.rows))
        self.cmd_set = msg2
        # my_matrix = np.array(msg2.data).reshape(10, 10)
        # aaa = my_matrix[0,:]
        self.get_logger().info("My litiener is array: %s" % str(msg2))


    def timer_callback(self,msg2):                                     # 创建定时器周期执行的回调函数

        # self.get_logger().info("recieved log mag2")
        # self.get_logger().info('My litiener is: %d and %d' % (msg2.cols, msg2.rows))
        robot_state = np.zeros((9, 10))
        robot_state[0,:] = np.array(msg2.x)
        # my_matrix = np.array(msg2.data).reshape(10, 10)
        # aaa = my_matrix[0,:]
        self.get_logger().info("My litiener is array: %s" % str(robot_state))


def main(args=None):                                 # ROS2节点主入口main函数
    rclpy.init(args=args)                            # ROS2 Python接口初始化
    Getmsg = Fcn_Getmsg("get_my_msg")
    rclpy.spin(Getmsg)
    rclpy.shutdown()








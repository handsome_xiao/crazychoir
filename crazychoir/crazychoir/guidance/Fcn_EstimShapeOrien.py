import numpy as np

def Fcn_EstimShapeOrien(sim_param, neigh_mtr, shape_state):
    # set proportional gain
    kappa_1 = 0.8
    kappa_2 = 1.5
    
    # variable substitution
    t = sim_param['t']
    a_mtr = neigh_mtr['a_mtr']
    swarm_size = sim_param['swarm_size']
    
    # get the number of neighbors
    neigh_num = np.sum(a_mtr, axis=1)
    neigh_num[neigh_num==0] = 1
    
    # reset control gain
    kappa_2 = kappa_2 + (kappa_1 / kappa_2) / neigh_num
    
    # calculate consensus of all robots
    f_conse = CalConseForce(kappa_1, kappa_2, swarm_size, a_mtr, shape_state)
    
    # calculate estimate force in different cases
    # calculate damping force for leader-less cases
    f_damp = CalDampForce(shape_state)
    
    # get estimation command
    cmd_set = -f_conse + f_damp
    
    # update estimation movements
    state = UpdateMovement(t, cmd_set, shape_state)
    
    return state

def CalDampForce(shape_state):
    force = -shape_state['hvel_set']
    return force

def CalConseForce(kappa_1, kappa_2, swarm_size, a_mtr, shape_state):
    # variable substitution
    head_set = shape_state['head_set']
    hvel_set = shape_state['hvel_set']
    
    # calculate postion-estimation error
    head_mtr = np.kron(head_set, np.ones((swarm_size, 1)))
    head_rel = (head_mtr.T - head_mtr) * a_mtr
    head_err = np.sum(head_rel, axis=1)
    
    # calculate velocity-estimation error
    hvel_mtr = np.kron(hvel_set, np.ones((swarm_size, 1)))
    hvel_rel = (hvel_mtr.T - hvel_mtr) * a_mtr
    hvel_err = np.sum(hvel_rel, axis=1)
    
    # get consensus force
    force = kappa_1 * head_err + kappa_2 * hvel_err
    
    return force

def UpdateMovement(t, cmd_set, shape_state):
    state = {}
    state['head_set'] = shape_state['head_set'] + shape_state['hvel_set'] * t + (t * t / 2) * shape_state['hacc_set']
    state['hvel_set'] = shape_state['hvel_set'] + cmd_set * t
    state['hacc_set'] = cmd_set
    
    return state

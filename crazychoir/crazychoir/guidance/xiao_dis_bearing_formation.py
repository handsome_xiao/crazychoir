from typing import Callable
import threading
import numpy as np
from geometry_msgs.msg import Vector3
from std_msgs.msg import Empty
from trajectory_msgs.msg import JointTrajectory as Trajectory, JointTrajectoryPoint as TrajectoryPoint
import sys
import rclpy
import matplotlib.pyplot as plt
# 添加其他路径到系统路径
# sys.path.append('/home/issl/ws_crazy/build/crazychoir/crazychoir/guidance')
from .modle.get_n3 import get_n
from .modle.xiao_4_force import Cal_4_force
from .distributed_control import DistributedControl
from .modle import Lib_Init ,Lib_Update ,Lib_Shape
from .modle import Fcn_EstimShapeOrien,Fcn_EstimShapePosti,Fcn_GetNeighborSet,Lib_Curves

class XiaoDisBearingFormation(DistributedControl):
    """
    Formation Control 

    Implements a bearing-based formation control law for second order systems.
    """
    def __init__(self, update_frequency: float,
                 pose_handler: str=None, pose_topic: str=None, pose_callback: Callable=None, input_topic = 'acceleration'):
        super().__init__(update_frequency, pose_handler, pose_topic, pose_callback, input_topic)

        self.get_logger().info('Guidance {} started'.format(self.agent_id))
        self.agent_id = self.get_parameter('agent_id').value
        self.is_leader = self.get_parameter('is_leader').value
        self.agent_dim = self.get_parameter('dd').value
        self.ort_proj_array = self.get_parameter('ort_proj').value
        self.init_pos = self.get_parameter('init_pos').value
        self.vicon = self.get_parameter('vicon').value
        self.step = 0
        # reshape ortogonal projection Matrix
        self.ort_proj = np.reshape(self.ort_proj_array, ((self.agent_dim,self.n_agents*self.agent_dim)))

        if self.vicon:
            # Vicon Parameters
            self.bearing_prop_gain = np.array([3,3,10])
            self.bearing_deriv_gain = np.array([3,3,3])
            self.bearing_int_gain = np.array([0,0,1])

            self.prop_gain = np.array([720, 720, 3000])*1e-3*10
            self.deriv_gain = np.array([120, 120, 400])*1e-3*30
            self.int_gain = np.array([0, 0, 1000])*1e-3
        else:
            # Webots Parameters
            # NOTE: if HUGE formation use these values:
            # self.bearing_prop_gain = np.array([1,1,1])*0.1*0.7
            # self.bearing_deriv_gain = np.array([1,1,1])*0.5*0.7
            self.bearing_prop_gain = np.array([1,1,1])*0.1*2
            self.bearing_deriv_gain = np.array([1,1,1])*0.5*2
            self.bearing_int_gain = np.array([0,0,0])

            self.prop_gain = np.array([400, 400, 1250])*1e-3
            self.deriv_gain = np.array([200, 200, 400])*1e-3
            self.int_gain = np.array([0, 0, 0])*1e-3
            
        # Subscription to gui commands
        self.take_off_trigger_subscription = self.create_subscription(Empty, '/takeoff', self.start_take_off, 10)
        self.land_trigger_subscription = self.create_subscription(Empty, '/land', self.start_land, 10)
        self.experiment_trigger_subscription = self.create_subscription(Empty, '/experiment_trigger', self.start_experiment, 10)

        # Subscription to trajectory topic
        self.publishers_traj_params = self.create_publisher(Trajectory, 'traj_params', 1)

        # FSM Variables
        self.height = 1.0
        self.formation = False
        self.takeoff = False
        self.takeoff_started = False
        self.takeoff_pos = np.zeros(3)
        self.landing = False
        self.land_started = False
        self.landing_pos = np.zeros(3)

        # Integral action settings
        # self.delta_int = 0.001 # [ms]
        self.delta_int = 0.1/update_frequency # [ms]
        self.err_int_bearinig = np.zeros(3)
        self.err_int = np.zeros(3)

        # xiaojvlong init
        self.litsten = self.create_subscription(Vector3, "cmd_set", self.litsten_callback ,10)
        self.litsten#防止被垃圾回收
        self.cmd_set = Vector3()
        self.cmd_set.x = 1.0
        self.cmd_set.y = 2.0
        self.cmd_set.z = 3.0
        n = get_n()
        self.Run_time = 10
        self.Swarm_size = 10
        self.Sim_range = [-2.5, 2.5, -2, 2]
        self.Sim_param = Lib_Init.InitSimuParam(self.Run_time, self.Swarm_size)
        self.scale =2
        # initialize robots' states (including position and velocity)
        self.Robot_state = {
            'pos_set': np.array([[-0.6, 0.5], [0, 0.6], [0.5, 0.5], [-0.7, -0.1], [0.2, -0.1],
                                [0.6, 0.1], [-0.6, -0.6], [0, -0.5], [0.6, -0.6], [-0.2, 0.1]]).T,
            'vel_set': np.zeros((2, self.Swarm_size)),
            'acc_set': np.zeros((2, self.Swarm_size)),
            'head_set': np.zeros(self.Swarm_size),
            'hvel_set': np.zeros(self.Swarm_size)
        }
        # type: 1-T, 2-I, 3-E
        self.type = 1
        # load shape image
        image = Lib_Shape.LoadShapeImage(self.type)
        self.Gray_image = image['gray_mtr']
        
        # initialize formation shape
        self.Gray_mtr, self.Gray_info = Lib_Shape.InitFormation(self.Sim_param, self.Gray_image)
        # initialize shape states
        self.Shape_state = Lib_Shape.InitShapeState(self.Sim_param, self.Robot_state)
        self.Rcent_state = Lib_Shape.GetShapeCenter(self.Swarm_size, self.Shape_state)
        # initialize control inputs
        # initalize record states
        # self.Record_state = Lib_Init.InitRecordState(Sim_param, Robot_state, Shape_state, Rcent_state, cmd_set)


        # self.get_logger().info("My litiener is array: %s" % str(n))
        # self.timer = self.create_timer(1, self.evaluate_input_copy) 


    
    def litsten_callback(self,msg2):                                  
        self.get_logger().info("测试callback111\n")
        # self.get_logger().info("recieved log mag2")
        # self.get_logger().info('My litiener is: %d and %d' % (msg2.cols, msg2.rows))
        # self.cmd_set = msg2
        # my_matrix = np.array(msg2.data).reshape(10, 10)
        # aaa = my_matrix[0,:]
        # self.get_logger().info("My litiener is array: %s" % str(msg2))

    def litsten_thread(self):
        def litsten_callback(msg2):                                  
            print("123"*5)
            # self.get_logger().info("recieved log mag2")
            # self.get_logger().info('My litiener is: %d and %d' % (msg2.cols, msg2.rows))
            # self.cmd_set = msg2
            # my_matrix = np.array(msg2.data).reshape(10, 10)
            # aaa = my_matrix[0,:]
            # self.get_logger().info("My litiener is array: %s" % str(msg2))
        self.litsten = self.create_subscription(Vector3, "cmd_set", litsten_callback ,10)
        self.litsten
        self.get_logger().info("测试listen\n")
        # rclpy.spin(self.litsten)  # 运行消息循环
        # litsten_thread = threading.Thread(target=litsten_thread)
        # litsten_thread.start()    

    # def evaluate_input(self, neigh_data):
    #     u = np.zeros(3)

    #     if self.current_pose.position is not None and self.current_pose.velocity is not None:
    #         if self.takeoff:
    #             if self.takeoff_started:
    #                 if self.is_leader:
    #                     self.leaders_takeoff(self.height)

    #                 self.err_int = np.zeros(3)
    #                 self.takeoff_started = False
    #                 x_des, y_des, z_des = self.current_pose.position
    #                 self.takeoff_pos = np.array([x_des,y_des,z_des])

    #             if self.takeoff_pos[2] <= self.height:
    #                 self.takeoff_pos[2] += 1/self.update_frequency*0.2

    #             err_pos = self.current_pose.position - self.takeoff_pos
    #             err_vel = self.current_pose.velocity
    #             self.err_int += self.delta_int*err_pos

    #             # Sent only if is follower -> See self.send_input()
    #             u = - (self.prop_gain*err_pos + self.deriv_gain*err_vel + self.int_gain * self.err_int)

    #         elif self.landing:
    #             if self.land_started:
    #                 if self.is_leader:
    #                     self.leaders_landing()

    #                 self.land_started = False
    #                 x_des, y_des, z_des = self.current_pose.position
    #                 self.landing_pos = np.array([x_des,y_des,z_des])
            
    #             if self.landing_pos[2] > 0.01:
    #                 self.landing_pos[2] -= 1/self.update_frequency*0.2

    #             err_pos = self.current_pose.position - self.landing_pos
    #             err_vel = self.current_pose.velocity
    #             self.err_int += self.delta_int*err_pos
                
    #             # Sent only if is follower -> See self.send_input()
    #             u = - (self.prop_gain*err_pos + self.deriv_gain*err_vel + self.int_gain * self.err_int)

    #         elif self.formation:
    #             if not self.is_leader:
    #                 i = self.agent_id
    #                 dd = self.agent_dim
    #                 N = self.n_agents
    #                 for j, neigh_pose in neigh_data.items():
    #                     err_pos = self.current_pose.position - neigh_pose.position
    #                     err_vel = self.current_pose.velocity - neigh_pose.velocity
    #                     self.err_int_bearinig += self.delta_int*err_pos

    #                     P_ij = self.ort_proj[:,j*dd:(j+1)*dd]
    #                     u += - P_ij @ (self.bearing_prop_gain * err_pos + self.bearing_deriv_gain * err_vel + self.bearing_int_gain * self.err_int_bearinig)
    #                     # cmd_set = self.evaluate_input_copy()
    #                     # u[0] = cmd_set[0]
    #                     # u[1] = cmd_set[1]
    #                     self.get_logger().info("My cmd_set is: %s" % str(u))
    #                     # u = np.array([0.2, 0.2, 0])
    #     else:
    #         self.get_logger().warn('Pose or Velocity are None')
        
    #     return u



    def evaluate_input(self, neigh_data):
        u = np.zeros(3)
        
        if self.current_pose.position is not None and self.current_pose.velocity is not None:
            if self.takeoff:
                if self.takeoff_started:
                    if self.is_leader:
                        self.leaders_takeoff(self.height)

                    self.err_int = np.zeros(3)
                    self.takeoff_started = False
                    x_des, y_des, z_des = self.current_pose.position
                    self.takeoff_pos = np.array([x_des,y_des,z_des])

                if self.takeoff_pos[2] <= self.height:
                    self.takeoff_pos[2] += 1/self.update_frequency*0.2

                err_pos = self.current_pose.position - self.takeoff_pos
                err_vel = self.current_pose.velocity
                self.err_int += self.delta_int*err_pos

                # Sent only if is follower -> See self.send_input()
                u = - (self.prop_gain*err_pos + self.deriv_gain*err_vel + self.int_gain * self.err_int)
                # self.get_logger().info('single_robot_state: "%s"' % str(u))
                with open('data.txt', 'a') as file:
                    u = np.around(u, decimals=5)
                    data = str(u)
                    file.write('takeoff' + data + '\n')
                    # 刷新缓冲区，确保数据被写入文件
                    file.flush()
                    file.close()
                
            elif self.landing:
                if self.land_started:
                    if self.is_leader:
                        self.leaders_landing()

                    self.land_started = False
                    x_des, y_des, z_des = self.current_pose.position
                    self.landing_pos = np.array([x_des,y_des,z_des])
            
                if self.landing_pos[2] > 0.01:
                    self.landing_pos[2] -= 1/self.update_frequency*0.2

                err_pos = self.current_pose.position - self.landing_pos
                err_vel = self.current_pose.velocity
                self.err_int += self.delta_int*err_pos
                
                # Sent only if is follower -> See self.send_input()
                u = - (self.prop_gain*err_pos + self.deriv_gain*err_vel + self.int_gain * self.err_int)

            elif self.formation:
                if not self.is_leader:
                    i = self.agent_id
                    dd = self.agent_dim
                    N = self.n_agents
                    for j, neigh_pose in neigh_data.items():
                        err_pos = self.current_pose.position - neigh_pose.position
                        err_vel = self.current_pose.velocity - neigh_pose.velocity
                        self.err_int_bearinig += self.delta_int*err_pos

                        P_ij = self.ort_proj[:,j*dd:(j+1)*dd]
                        u += - P_ij @ (self.bearing_prop_gain * err_pos + self.bearing_deriv_gain * err_vel + self.bearing_int_gain * self.err_int_bearinig)
                        # self.step += 1
                        # if self.step%5 == 0:
                        # if self.agent_id == 4:
                            # self.get_logger().info("helloworld")
                            # # print(u)
                            # self.get_logger().info("My cmd_set is: %.4f, %.4f, %.4f" % (u[0], u[1], u[2]))
                        # with open('data.txt', 'a') as file:
                        #     u = np.around(u, decimals=5)
                        #     data = str(u)
                        #     file.write('formation' + data + '\n')
                        #     # 刷新缓冲区，确保数据被写入文件
                        #     file.flush()
                        #     file.close()
                        # cmd_set = self.evaluate_input_copy()
                        # self.get_logger().info("helloworld")
                        # u[0] = cmd_set[0]
                        # u[1] = cmd_set[1]
                        # self.get_logger().info("My cmd_set is: %s" % str(self.cmd_set))
                        # u = np.array([0.2, 0.2, 0])
        else:
            self.get_logger().warn('Pose or Velocity are None')

        return u




    def evaluate_input_copy(self):
        deform_step1 = 1000
        deform_step2 = 1500
        self.step += 1
        result = np.zeros((2, 1)) 
        for step in range(1):
            
            # if step == deform_step1:
            #     # Lib_Curves.DrawFinalSwarm(step , Sim_param, Record_state)
            #     Gray_image = Lib_Shape.LoadShapeImage(2)
            #     Gray_image = Gray_image['gray_mtr']
            #     Gray_mtr, Gray_info = Lib_Shape.InitFormation(self.Sim_param, Gray_image)
            # if step == deform_step2:
            #     # Lib_Curves.DrawFinalSwarm(step , Sim_param, Record_state)
            #     Gray_image = Lib_Shape.LoadShapeImage(3)
            #     Gray_image = Gray_image['gray_mtr']
            #     Gray_mtr, Gray_info = Lib_Shape.InitFormation(self.Sim_param, Gray_image)

            # get neighboring set
            Neigh_mtr = Fcn_GetNeighborSet.Fcn_GetNeighborSet(self.Sim_param, self.Robot_state)
            # estimate shape position
            state = Fcn_EstimShapePosti.Fcn_EstimShapePosti(self.Sim_param, Neigh_mtr, self.Shape_state)
            self.Shape_state['pos_set'] = state['pos_set']
            self.Shape_state['vel_set'] = state['vel_set']
            self.Shape_state['acc_set'] = state['acc_set']

            # estimate shape orientation
            state = Fcn_EstimShapeOrien.Fcn_EstimShapeOrien(self.Sim_param, Neigh_mtr, self.Shape_state)
            self.Shape_state['head_set'] = state['head_set']
            self.Shape_state['hvel_set'] = state['hvel_set']
            self.Shape_state['hacc_set'] = state['hacc_set']



            # get dynamic formation
            # Rcent_state = Lib_Shape.GetShapeCenter(self.Swarm_size, self.Shape_state)
            Form_dyn = Lib_Shape.GetDynFormation(self.Swarm_size, self.Gray_mtr, self.Gray_info, self.Shape_state)
            # Lib_Curves.ShowMatrix(Form_dyn['shape_value'])

            
            # self.get_logger().info("第%s个智能体,第%s次循环" % (str(self.agent_id),str(self.step)))
            robot_state, vel_cmd, cmd_set = Cal_4_force(self.Sim_param, self.Robot_state, self.Shape_state, Form_dyn, self.Gray_info,Neigh_mtr,self.Swarm_size) 
            # # update robots' motion
            # robot_state['pos_set'] = Lib_Update.UpdRobotMotion(self.Sim_param, robot_state, vel_cmd)


            # self.Robot_state = robot_state
            # # 动态显示
            # if step%50 == 0 & self.agent_id == 2:
            # # if self.step%50 == 0:
            #     self.get_logger().info("显示当前飞机队形")
            #     # Lib_Curves.DrawScatter(range(10) , Shape_state['head_set'])
            #     if self.agent_id == 10:
            #         count_id = 9
            #     else:
            #         count_id = self.agent_id
            #     result = cmd_set[:,count_id]
                
            #     # self.uu = np.concatenate((u_temp, b))
                
            #     Lib_Curves.DrawScatter(self.Robot_state['pos_set'][0, :] , self.Robot_state['pos_set'][1, :])
            # self.get_logger().info("My control u is:\n %s" % str(cmd_set))

        
        # if self.agent_id == 2:
        # Lib_Curves.DrawRobotState(self.Robot_state)
        return cmd_set









    def start_experiment(self, _):
        self.get_logger().info('Starting experiment')
        self.landing = False
        self.takeoff = False
        self.formation = True

    def start_take_off(self, _):
        self.get_logger().info('Starting Take-Off')
        self.landing = False
        self.formation = False
        self.takeoff = True
        self.takeoff_started = True

    def start_land(self, _):
        self.get_logger().info('Starting Landing')
        self.takeoff = False
        self.formation = False
        self.landing = True
        self.land_started = True

    def leaders_takeoff(self, z_des):
        msg = Trajectory()
        name = 'Agent_{}'.format(self.agent_id)

        point = TrajectoryPoint()
        x = self.init_pos[0]
        y = self.init_pos[1]
        z = z_des
        duration_s = 5
        point.positions = [x,y,z]
        point.velocities = [0.0,0.0,0.0]
        point.accelerations = [0.0,0.0,0.0]
        point.effort = [0.0,0.0,0.0]
        point.time_from_start.sec = int(duration_s)
        point.time_from_start.nanosec = int(0)

        msg.joint_names.append(name)
        msg.points.append(point)

        self.publishers_traj_params.publish(msg)

    def leaders_landing(self):
        msg = Trajectory()
        name = 'Agent_{}'.format(self.agent_id)

        point = TrajectoryPoint()
        x = self.current_pose.position[0]
        y = self.current_pose.position[1]
        z = 0.01
        duration_s = 5
        point.positions = [x,y,z]
        point.velocities = [0.0,0.0,0.0]
        point.accelerations = [0.0,0.0,0.0]
        point.effort = [0.0,0.0,0.0]
        point.time_from_start.sec = int(duration_s)
        point.time_from_start.nanosec = int(0)

        msg.joint_names.append(name)
        msg.points.append(point)

        self.publishers_traj_params.publish(msg)

    def send_input(self, u):
        msg = Vector3()

        msg.x = u[0]
        msg.y = u[1]
        msg.z = u[2]
        if not self.is_leader:
            self.publisher_.publish(msg)
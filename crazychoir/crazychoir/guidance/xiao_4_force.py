from .import Fcn_CalAlignForce,Fcn_CalAvoidForce,Fcn_CalFormForce,Fcn_CalStabForce,Fcn_TransVelCommand
def Cal_4_force(Sim_param, Robot_state, Shape_state, Form_dyn, Gray_info,Neigh_mtr,Swarm_size):    
    # calculate shape-forming force
    f_form, Role_set = Fcn_CalFormForce.Fcn_CalFormForce(Sim_param, Robot_state, Shape_state, Form_dyn, Gray_info)

    # calculate shape-stablizing force
    f_stab = Fcn_CalStabForce.Fcn_CalStabForce(Sim_param, Sim_param['r_stab'], Role_set, Robot_state, Form_dyn)

    # calculate collision-avoid force
    f_avoid = Fcn_CalAvoidForce.Fcn_CalAvoidForce(Sim_param, Neigh_mtr)

    # calculate velocity-alignement force
    f_align = Fcn_CalAlignForce.Fcn_CalAlignForce(Swarm_size, Neigh_mtr, Robot_state, Shape_state)

    # calculate control command
    cmd_set = f_form + f_stab + f_avoid + f_align

    # tranform acceleration to velocity command
    robot_state, vel_cmd = Fcn_TransVelCommand.transform_vel_command(Sim_param, Robot_state, cmd_set)
    return robot_state, vel_cmd ,cmd_set
import numpy as np

def Fcn_CalAvoidForce(sim_param, neigh_mtr):
    # set proportional gain
    kappa = 60
    
    # variable substitution
    r_safe = sim_param['r_safe']
    r_avoid = sim_param['r_avoid']
    a_mtr = neigh_mtr['a_mtr'].astype(int)
    d_mtr = neigh_mtr['d_mtr']
    rx_mtr = neigh_mtr['rx_mtr']
    ry_mtr = neigh_mtr['ry_mtr']
    
    # calculate force
    chi = 1
    dis_mtr = d_mtr + (np.logical_not(a_mtr).astype(int)) * r_avoid
    force_mtr = cal_repulse_field(dis_mtr, r_safe, r_avoid, chi)
    # d_mtr_int = d_mtr.astype(int)
    dis_mtr = d_mtr + (np.logical_not(d_mtr).astype(int))
    unit_x = -rx_mtr / dis_mtr
    unit_y = -ry_mtr / dis_mtr
    force_x = np.sum(force_mtr * unit_x, axis=1)
    force_y = np.sum(force_mtr * unit_y, axis=1)
    force = kappa * np.vstack((force_x.T, force_y.T))
    
    return force


def cal_repulse_field(x, r, R, kappa):
    x[x<2*r] = 2*r
    x[x>R] = R
    y = 1 - (-kappa*np.cos((np.pi/(R-2*r))*(x-2*r))**2 - 2*np.cos((np.pi/(R-2*r))*(x-2*r)) + 2 + kappa) / 4
    
    return y

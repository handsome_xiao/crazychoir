import matplotlib.pyplot as plt


def DrawFinalSwarm(step , Sim_param,Record_state ):
    print("DrawFinalSwarm")
    plt.figure(6)
    # step = Sim_param['max_step']
    # 定义离散点的 x 和 y 坐标
    x = Record_state['pset_rec'][0, :, step-1] 
    y = Record_state['pset_rec'][1, :, step-1] 
    # 绘制离散点
    plt.scatter(x, y)
    # 设置图表标题和坐标轴标签
    plt.title('DrawFinalSwarm')
    plt.xlabel('X')
    plt.ylabel('Y')
    plt.axis('equal')
    # 显示图形
    plt.draw()
    # 暂停两秒
    plt.pause(2)
    # 关闭图形窗口
    plt.close()

def DrawRobotState(Record_state ):
    plt.figure(2)
    # step = Sim_param['max_step']
    # 定义离散点的 x 和 y 坐标
    x = Record_state['pos_set'][0, :] 
    y = Record_state['pos_set'][1, :] 
    # 绘制离散点
    plt.scatter(x, y)
    # 设置图表标题和坐标轴标签
    plt.title('DrawRobotState')
    plt.xlabel('X')
    plt.ylabel('Y')
    plt.axis('equal')
    # 显示图形
    plt.draw()
    # 暂停两秒
    plt.pause(5)
    # 关闭图形窗口
    plt.close()   

def ShowMatrix(matrix):
    print("show_matrix")
    # 使用 imshow 绘制矩阵
    plt.imshow(matrix, cmap='gray')

    # 添加颜色条
    plt.colorbar()
    plt.title('show_matrix')
    # 显示图形
    plt.draw()
    # 暂停两秒
    plt.pause(2)
    # 关闭图形窗口
    plt.close()
    

def DrawScatter(x,y ):
    print("DrawScatter")
    # plt.figure(6)
    plt.scatter(x, y)
    # 设置图表标题和坐标轴标签
    plt.title('DrawScatter')
    plt.xlabel('X')
    plt.ylabel('Y')
    # plt.axis('equal')
    # 显示图形
    # 设置x轴和y轴的范围
    plt.xlim(-2.5, 2.5)   # 设置x轴范围为2到8
    plt.ylim(-2, 2)  # 设置y轴范围为-1到1
    plt.draw()
    # 暂停两秒
    plt.pause(0.2)
    # 关闭图形窗口
    # plt.close()

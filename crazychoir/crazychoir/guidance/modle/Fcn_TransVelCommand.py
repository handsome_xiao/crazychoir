import numpy as np

def transform_vel_command(sim_param, robot_state, cmd_set):
    # variable substitution
    t = sim_param['t']
    vel_max = sim_param['vel_max']
    
    # command transformation
    state = {}
    state['pos_set'] = robot_state['pos_set']
    state['vel_set'] = robot_state['vel_set'] + cmd_set * t
    state['acc_set'] = cmd_set
    state['head_set'] = np.arctan2(state['vel_set'][1, :], state['vel_set'][0, :])
    state['hvel_set'] = (state['head_set'] - robot_state['head_set']) / t
    
    # limit the maximum velocity
    vel_scale = np.sqrt(state['vel_set'][0, :]**2 + state['vel_set'][1, :]**2)
    index = np.where(vel_scale > vel_max)[0]
    state['vel_set'][:, index] = vel_max * np.array([np.cos(state['head_set'][index]), np.sin(state['head_set'][index])])
    
    # correct acceleration according to the maximum velocity
    state['acc_set'][:, index] = (state['vel_set'][:, index] - robot_state['vel_set'][:, index]) / t
    
    # generate velocity command
    vel_cmd = state['vel_set']
    
    return state, vel_cmd

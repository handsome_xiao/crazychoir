import numpy as np

def Fcn_CalAlignForce(swarm_size, neigh_mtr, robot_state, shape_state):
    # set proportional gain
    kappa_1 = 1.0
    kappa_2 = 1.0
    
    # variable substitution
    vel_set = robot_state['vel_set']
    a_mtr = neigh_mtr['a_mtr']
    
    # get relative velocity among robots
    vx_mtr = np.tile(vel_set[0], (swarm_size, 1)).T
    vy_mtr = np.tile(vel_set[1], (swarm_size, 1)).T
    vx_rel = (vx_mtr - vx_mtr.T) * a_mtr
    vy_rel = (vy_mtr - vy_mtr.T) * a_mtr
    
    # velocity compensation
    gvel_set = shape_state['vel_set']
    comp_set = gvel_set - vel_set
    
    # calculate force
    sum_x = np.sum(vx_rel, axis=1)
    sum_y = np.sum(vy_rel, axis=1)
    aaa = np.vstack((sum_x, sum_y))
    force = -kappa_1 * np.vstack((sum_x, sum_y)) + kappa_2 * comp_set + shape_state['acc_set']
    
    return force

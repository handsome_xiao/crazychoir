import numpy as np

def Fcn_EstimShapePosti(sim_param, neigh_mtr, shape_state):
    # set proportional gain
    kappa_1 = 1.5
    kappa_2 = 3.0
    
    # variable substitution
    t = sim_param['t']
    a_mtr = neigh_mtr['a_mtr']
    swarm_size = sim_param['swarm_size']
    
    # get the number of neighbors
    neigh_num = np.sum(a_mtr, axis=1)
    neigh_num[neigh_num==0] = 1
    
    # reset control gain
    kappa_2 = kappa_2 + (kappa_1 / kappa_2) / neigh_num
    
    # calculate consensus of all robots
    f_conse = CalConseForce(kappa_1, kappa_2, swarm_size, a_mtr, shape_state)
    
    # calculate damping force for leader-less cases
    f_damp = CalDampForce(shape_state)
    
    # get estimation command
    cmd_set = -f_conse + f_damp
    
    # update estimation movements
    state = UpdateMovement(t, cmd_set, shape_state)
    
    return state

def CalDampForce(shape_state):
    force = -shape_state['vel_set']
    return force

def CalConseForce(kappa_1, kappa_2, swarm_size, a_mtr, shape_state):
    # variable substitution
    pos_set = shape_state['pos_set']
    vel_set = shape_state['vel_set']
    
    # calculate postion-estimation error
    px_mtr = np.kron(pos_set[0, :], np.ones((swarm_size, 1)))
    py_mtr = np.kron(pos_set[1, :], np.ones((swarm_size, 1)))
    px_rel = (px_mtr.T - px_mtr) * a_mtr
    py_rel = (py_mtr.T - py_mtr) * a_mtr
    pos_err = np.zeros((2, swarm_size))
    pos_err[0, :] = np.sum(px_rel, axis=1)
    pos_err[1, :] = np.sum(py_rel, axis=1)
    
    # calculate velocity-estimation error
    vx_mtr = np.kron(vel_set[0, :], np.ones((swarm_size, 1)))
    vy_mtr = np.kron(vel_set[1, :], np.ones((swarm_size, 1)))
    vx_rel = (vx_mtr.T - vx_mtr) * a_mtr
    vy_rel = (vy_mtr.T - vy_mtr) * a_mtr
    vel_err = np.zeros((2, swarm_size))
    vel_err[0, :] = np.sum(vx_rel, axis=1)
    vel_err[1, :] = np.sum(vy_rel, axis=1)
    
    # get consensus force
    force = kappa_1 * pos_err + kappa_2 * vel_err
    
    return force

def UpdateMovement(t, cmd_set, shape_state):
    state = {}
    state['pos_set'] = shape_state['pos_set'] + shape_state['vel_set'] * t + (t * t / 2) * shape_state['acc_set']
    state['vel_set'] = shape_state['vel_set'] + cmd_set * t
    state['acc_set'] = cmd_set
    
    return state

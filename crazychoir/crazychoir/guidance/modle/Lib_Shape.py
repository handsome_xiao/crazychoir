import numpy as np
import matplotlib.pyplot as plt
from scipy.io import loadmat
# FUNCTION: shape-formation library
# def Lib_ShapeFunctions():
#     lib = {
#         'LoadShapeImage': LoadShapeImage,
#         'InitShapeState': InitShapeState,
#         'GetShapeCenter': GetShapeCenter,
#         'InitFormation': InitFormation,
#         'GetDynFormation': GetDynFormation
#     }
#     return lib
# FUNCTION: get dynamic formation
def GetDynFormation_init(swarm_size, shape_mtr, shape_info, shape_state):
    # variable substitution
    rn = shape_info['rn']
    cn = shape_info['cn']
    head_set = np.array(shape_state['head_set'])
    head_set = shape_state['head_set']
    pos_set = shape_state['pos_set']
    
    # initialize output
    shape_x = np.zeros((rn, cn, swarm_size))
    shape_y = np.zeros((rn, cn, swarm_size))
    shape_value = shape_mtr[:, :, 2]
    
    # transform coordinate
    for i in range(swarm_size):
        temp_mtr = np.zeros((rn, cn, 2))
        #这里作了修改，head_set是标量而不是矩阵，无法索引
        temp_mtr[:, :, 0] = shape_mtr[:, :, 0] * np.cos(head_set) - shape_mtr[:, :, 1] * np.sin(head_set)
        temp_mtr[:, :, 1] = shape_mtr[:, :, 0] * np.sin(head_set) + shape_mtr[:, :, 1] * np.cos(head_set)
        # 这里作了修改，pos_set是一个二维向量
        shape_x[:, :, i] = temp_mtr[:, :, 0] + pos_set[0]
        shape_y[:, :, i] = temp_mtr[:, :, 1] + pos_set[1]
    
    # assign output
    shape_dyn = {
        'shape_x': shape_x,
        'shape_y': shape_y,
        'shape_value': shape_value,
        'shape_head': head_set,
        'size': swarm_size
    }
    return shape_dyn

# FUNCTION: get dynamic formation
def GetDynFormation(swarm_size, shape_mtr, shape_info, shape_state):
    # variable substitution
    rn = shape_info['rn']
    cn = shape_info['cn']
    head_set = np.array(shape_state['head_set'])
    head_set = shape_state['head_set']
    pos_set = shape_state['pos_set']
    
    # initialize output
    shape_x = np.zeros((rn, cn, swarm_size))
    shape_y = np.zeros((rn, cn, swarm_size))
    shape_value = shape_mtr[:, :, 2]
    
    # transform coordinate
    for i in range(swarm_size):
        temp_mtr = np.zeros((rn, cn, 2))
        #这里作了修改，head_set是标量而不是矩阵，无法索引
        temp_mtr[:, :, 0] = shape_mtr[:, :, 0] * np.cos(head_set[i]) - shape_mtr[:, :, 1] * np.sin(head_set[i])
        temp_mtr[:, :, 1] = shape_mtr[:, :, 0] * np.sin(head_set[i]) + shape_mtr[:, :, 1] * np.cos(head_set[i])
        # 这里作了修改，pos_set是一个二维向量
        shape_x[:, :, i] = temp_mtr[:, :, 0] + pos_set[0,i]
        shape_y[:, :, i] = temp_mtr[:, :, 1] + pos_set[1,i]
    
    # assign output
    shape_dyn = {
        'shape_x': shape_x,
        'shape_y': shape_y,
        'shape_value': shape_value,
        'shape_head': head_set,
        'size': swarm_size
    }
    return shape_dyn

# FUNCTION: initialize formation
def InitFormation(sim_param, image_mtr):
    # variable substitution
    r_avoid = sim_param['r_avoid']
    swarm_size = sim_param['swarm_size']
    gary_image = image_mtr
    # calculate parameters of shape image
    shape_info = {}
    shape_info['rn'], shape_info['cn'] = image_mtr.shape
    shape_info['cen_x'] = np.round(np.ceil(shape_info['cn'] / 2)).astype(int)
    shape_info['cen_y'] = np.round(np.ceil(shape_info['rn'] / 2)).astype(int)
    shape_info['black_num'] = np.sum(image_mtr == 0)
    shape_info['gray_num'] = np.sum(image_mtr < 1) - shape_info['black_num']
    temp_mtr = image_mtr[shape_info['cen_y'], :]
    # temp_mtr[temp_mtr == 0] = 2
    shape_info['gray_scale'] = 1 / np.min(temp_mtr[temp_mtr != 0])
    # shape_info['gray_scale'] = 1 / np.min(temp_mtr)
    # calculate formation size
    ratio = 0.8  # 1.22
    shape_info['grid'] = np.sqrt((np.pi / 4) * (swarm_size / shape_info['black_num'])) * r_avoid
    shape_info['grid'] = shape_info['grid'] * ratio
    
    # calculate formation-shape coordinates
    shape_mtr = np.ones((shape_info['rn'], shape_info['cn'], 3))
    temp_x = ((np.arange(shape_info['cn']) + 1) - shape_info['cen_x']) * shape_info['grid']
    temp_y = ((np.arange(shape_info['rn']) + 1) - shape_info['cen_y']) * shape_info['grid']
    shape_mtr[:, :, 0] = shape_mtr[:, :, 0] * temp_x
    shape_mtr[:, :, 1] = shape_mtr[:, :, 1] * np.flipud(temp_y[:, np.newaxis])
    shape_mtr[:, :, 2] = gary_image
    aaa = shape_mtr[:, :, 2]
    # plt.imshow(aaa, cmap='gray')
    # plt.colorbar()
    # plt.show()
    return shape_mtr, shape_info

# FUNCTION: get shape center 
def GetShapeCenter(swarm_size, robot_state):
    state = {}
    state['pos_set'] = np.sum(robot_state['pos_set'], axis=1).T / swarm_size
    state['vel_set'] = np.sum(robot_state['vel_set'], axis=1) / swarm_size
    state['head_set'] = np.sum(robot_state['head_set']) / swarm_size
    state['hvel_set'] = np.sum(robot_state['hvel_set']) / swarm_size
    return state

# FUNCTION: initialize shape states
def InitShapeState(sim_param, robot_state):
    # variable substitution
    swarm_size = sim_param['swarm_size']
    
    # generate initial states
    state = {
        'pos_set': robot_state['pos_set'],
        'vel_set': robot_state['vel_set'],
        'acc_set': robot_state['acc_set'],
        'head_set': robot_state['head_set'],
        'hvel_set': robot_state['hvel_set'],
        'hacc_set': np.zeros(swarm_size)
    }
    return state

# FUNCTION: load shape image
def LoadShapeImage(type):
    if type == 1:
        mat_file = '/home/issl/zaluan_shixi/formation_py_dis_big/ShapeImage/Image_letter_T.mat'
    if type == 2:
        mat_file = '/home/issl/zaluan_shixi/formation_py_dis_big/ShapeImage/Image_letter_I.mat'
    if type == 3:
        mat_file = '/home/issl/zaluan_shixi/formation_py_dis_big/ShapeImage/Image_letter_E.mat'  
    # 使用loadmat函数加载MAT文件
    image = loadmat(mat_file)
    # ShowMatrix(image['gray_mtr'])
    return image




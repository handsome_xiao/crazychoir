import numpy as np

def Fcn_CalFormForce(sim_param, robot_state, shape_state, shape_mtr, shape_info):
    # set proportional gain
    kappa = 10

    # variable substitution
    rn = shape_info['rn']
    cn = shape_info['cn']
    grid = shape_info['grid']
    cen_x = shape_info['cen_x']
    cen_y = shape_info['cen_y']
    
    shape_x = shape_mtr['shape_x']
    shape_y = shape_mtr['shape_y']
    shape_value = shape_mtr['shape_value']
    
    r_body = sim_param['r_body']
    r_avoid = sim_param['r_avoid']
    swarm_size = sim_param['swarm_size']
    
    pos_set = robot_state['pos_set']
    fpos_set = shape_state['pos_set']
    fhead_set = shape_state['head_set']
    
    # calculate local goal point for each robot
    gray_set = np.zeros(swarm_size)
    goal_set = np.zeros((2, swarm_size))
    inside = np.zeros(swarm_size)
    
    for index in range(swarm_size):
        # locate agent in the local coordinate system
        location = TransGoalToLocal(pos_set[:,index], fpos_set[:,index], fhead_set[index], grid, cen_x, cen_y, rn, cn)
        
        # get gray level of this location
        gray_value = GetGrayValue(location, shape_value)
        gray_set[index] = gray_value
        
        # calculate the local goal point
        if gray_value == 1:
            goal_local = GetLocalGoalOut(location, pos_set[:,index], shape_x[:,:,index], shape_y[:,:,index], shape_value, rn)
        else:
            delta = sim_param['vel_max'] * sim_param['t']
            range_val = np.ceil(delta / grid) + 3
            goal_local = GetLocalGoalIn(location, pos_set[:,index], shape_x[:,:,index], shape_y[:,:,index], shape_value, rn, cn, range_val)
        
        goal_set[:,index] = goal_local
        
        # check whether robot is inside the shape
        range_val = 0 # max(1, round(r_avoid / grid))
        inside[index] = CheckInsideShape(gray_value, location, range_val, shape_value, rn, cn)
    
    # calculate gradient-descent direction
    perr_set = goal_set - pos_set
    norm_set = np.sqrt(perr_set[0,:]**2 + perr_set[1,:]**2)
    norm_set[norm_set == 0] = 1
    grad_set = perr_set[:,:] / norm_set[:]
    # calculate shape-forming force
    force = kappa * grad_set * GetGradientValue(gray_set)
    
    return force, inside


# Auxiliary functions

def CheckInsideShape(gray_value, location, range_val, shape_value, rn, cn):
    # get valid range
    min_r = max(1, location[0] - range_val)
    min_c = max(1, location[1] - range_val)
    max_r = min(rn, location[0] + range_val)
    max_c = min(cn, location[1] + range_val)
    min_r = int(min_r)
    max_r = int(max_r)
    min_c = int(min_c)
    max_c = int(max_c)
    # get valid matrix
    valid_mtr = shape_value[min_r-1:max_r, min_c-1:max_c]
    sum_temp = np.sum(valid_mtr)
    
    # check whether inside
    if gray_value == 0 and sum_temp == 0:
        inside = 1
    else:
        inside = 0
    
    return inside


def GetGradientValue(x):
    y = 1 - np.cos(np.pi * x)
    return y


def GetLocalGoalIn(location, pos_set, shape_x, shape_y, shape_value, rn, cn, range_val):
    # get local matrix for searching
    min_r = max(1, location[0] - range_val)
    max_r = min(rn, location[0] + range_val)
    min_c = max(1, location[1] - range_val)
    max_c = min(cn, location[1] + range_val)
    min_r = int(min_r)
    max_r = int(max_r)
    min_c = int(min_c)
    max_c = int(max_c)
    local_mtr = shape_value[(min_r-1):max_r, (min_c-1):max_c]
   
    
    # find the cell whose gray value is less than current gray value
    cen_r = location[0] - min_r
    cen_r = int(cen_r)
    cen_c = location[1] - min_c
    cen_c = int(cen_c)
    valid_mtr = np.zeros_like(local_mtr)
    gray_value = local_mtr[cen_r, cen_c]
    valid_mtr[local_mtr < gray_value] = 1
    
    temp_x = shape_x[min_r-1:max_r, min_c-1:max_c] - pos_set[0]
    temp_y = shape_y[min_r-1:max_r, min_c-1:max_c] - pos_set[1]
    dist_mtr = np.sqrt(temp_x**2 + temp_y**2) * valid_mtr
    dist_mtr *= valid_mtr
    dist_mtr[dist_mtr == 0] = np.inf
    min_val = np.min(dist_mtr)
    r_index, c_index = np.unravel_index(dist_mtr.argmin(), dist_mtr.shape)
    r_index += min_r
    c_index += min_c
    goal_x = shape_x[r_index, c_index]
    goal_y = shape_y[r_index, c_index]
    goal = np.array([goal_x, goal_y])
    
    return goal


def GetLocalGoalOut(location, pos_set, shape_x, shape_y, shape_value, rn):
    valid_mtr = shape_value.copy()
    valid_mtr[valid_mtr < 1] = 0
    
    temp_x = shape_x - pos_set[0]
    temp_y = shape_y - pos_set[1]
    dist_mtr = np.sqrt(temp_x**2 + temp_y**2) * (~valid_mtr)
    dist_mtr[dist_mtr == 0] = np.inf
    
    if location[2]:
        dist_mtr[location[0]-1, location[1]-1] = np.inf
    
    r_index, c_index = np.unravel_index(dist_mtr.argmin(), dist_mtr.shape)
    goal_x = shape_x[r_index, c_index]
    goal_y = shape_y[r_index, c_index]
    goal = np.array([goal_x, goal_y])
    
    return goal




def GetGrayValue(location, shape_value):
    if location[2] == 0:
        value = 1
    else:
        aaa = location[0].astype(int)
        bbb = location[1].astype(int)
        value = shape_value[location[0].astype(int), location[1].astype(int)]
    return value

def TransGoalToLocal(pos_set, fpos_set, fhead_set, grid, cen_x, cen_y, rn, cn):
    head_set = np.arctan2(pos_set[1] - fpos_set[1], pos_set[0] - fpos_set[0])
    azim_set = head_set - fhead_set
    azim_set = LimitAngle(azim_set)
    
    x_temp = (pos_set[0] - fpos_set[0])**2
    y_temp = (pos_set[1] - fpos_set[1])**2
    dist = np.sqrt(x_temp + y_temp)
    
    x_coor = np.round(dist * np.cos(azim_set) / grid)
    y_coor = np.round(dist * np.sin(azim_set) / grid)
    
    x_grid = x_coor + cen_x
    y_grid = rn - (y_coor + cen_y) + 1
    length = len(np.array([x_grid]))
   
    inside = np.zeros(length)
    index_x = np.where((x_grid >= 1) & (x_grid <= cn))[0]
    index_y = np.where((y_grid >= 1) & (y_grid <= rn))[0]
    inside[np.intersect1d(index_x, index_y)] = 1
    
    location = np.vstack([y_grid, x_grid, inside])
    return location

def LimitAngle(angle):
    angle = np.where(angle > np.pi, angle - 2 * np.pi, angle)
    angle = np.where(angle < -np.pi, angle + 2 * np.pi, angle)
    # angle[angle < -np.pi] = angle[angle < -np.pi] + 2 * np.pi
    return angle

import numpy as np

def Fcn_GetNeighborSet(param, robot_state):
    # variable substitution
    r_sense = param['r_sense']
    r_safe = param['r_safe']
    swarm_size = param['swarm_size']
    pos_set = robot_state['pos_set']
    
    # calculate distance matrix
    x_mtr = (pos_set[0].reshape(-1, 1) * np.ones((1, swarm_size)))
    y_mtr = (pos_set[1].reshape(-1, 1) * np.ones((1, swarm_size)))
    x_rel = x_mtr.T - x_mtr
    y_rel = y_mtr.T - y_mtr
    d_mtr = np.sqrt(x_rel**2 + y_rel**2)
    
    # calculate adjacent matrix
    a_temp = d_mtr <= (np.ones((swarm_size, swarm_size)) * r_sense)
    a_mtr = a_temp - np.eye(swarm_size)
    
    # output substitution
    output = {}
    output['a_mtr'] = a_mtr
    output['d_mtr'] = d_mtr
    output['rx_mtr'] = x_rel * a_mtr
    output['ry_mtr'] = y_rel * a_mtr
    
    return output

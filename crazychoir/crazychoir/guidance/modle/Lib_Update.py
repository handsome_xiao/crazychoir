import numpy as np

# FUNCTION: update library
def Lib_UpdateFunctions():
    lib = {
        'UpdRobotMotion': UpdRobotMotion,
        'UpdRecordStates': UpdRecordStates
    }
    return lib

# FUNCTION: update robots' motion
def UpdRobotMotion(sim_param, robot_state, cmd_set):
    # variable substitution
    t = sim_param['t']
    
    # update motion states
    pos_set = robot_state['pos_set'] + cmd_set * t
    return pos_set

# FUNCTION: update record states
def UpdRecordStates(step, record_state, robot_state, shape_state, rcent_state, cmd_set, Metric):
    # update robot motion
    record_state['pset_rec'][:, :, step+1] = robot_state['pos_set']
    record_state['vset_rec'][:, :, step+1] = robot_state['vel_set']
    record_state['ptra_rec'][:, step+1, :] = robot_state['pos_set']
    record_state['vtra_rec'][:, step+1, :] = robot_state['vel_set']
    record_state['cmd_rec'][:, step+1, :] = cmd_set
    
    # update shape-motion
    record_state['pcen_rec'][:, step+1] = rcent_state['pos_set']
    record_state['vcen_rec'][:, step+1] = rcent_state['vel_set']
    record_state['hecen_rec'][:, step+1] = rcent_state['head_set']
    record_state['hvcen_rec'][:, step+1] = rcent_state['hvel_set']
    record_state['pest_rec'][:, step+1, :] = shape_state['pos_set']
    record_state['vest_rec'][:, step+1, :] = shape_state['vel_set']
    record_state['aest_rec'][:, step+1, :] = shape_state['acc_set']
    record_state['heest_rec'][step+1, :] = shape_state['head_set']
    record_state['hvest_rec'][step+1, :] = shape_state['hvel_set']
    record_state['haest_rec'][step+1, :] = shape_state['hacc_set']
    
    # update metric
    # record_state['ent_rate'][step+1] = Metric['ent_rate']
    # record_state['dist_var'][step+1] = Metric['dist_var']
    record_state['ent_rate'][step+1] = None
    record_state['dist_var'][step+1] = None
    # update record states
    state = record_state
    return state

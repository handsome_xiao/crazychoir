# Change this path to your crazyflie-firmware folder
import sys
sys.path.append('/home/issl/crazyflie-firmware/build')
# /home/issl/ws_crazy/src/crazyflie-firmware/build
try:
    import cffirmware
except ImportError:
    raise ImportError('Set your crazyflie-firmware folder path in crazychoir/utils/__init__.py')
